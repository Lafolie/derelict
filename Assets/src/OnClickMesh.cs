﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OnClickMesh : MonoBehaviour {
    Pawn p;
	// Use this for initialization
	void Start () {
        p = gameObject.GetComponentInParent<Pawn>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseUpAsButton()
    {
        p.owner.game.focusPawn = p;
        p.owner.game.attackTarget = p;
    }

    void OnMouseEnter()
    {
        p.owner.game.hoverPawn = p;
    }

    void OnMouseExit()
    {
        if (p.owner.game.hoverPawn == p)
        {
            p.owner.game.hoverPawn = null;
        }
    }
}
