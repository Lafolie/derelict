﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {
    public Camera mainCamera;
    public float cameraSpeed = 8;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        bool inputQ = Input.GetButtonDown("RotateLeft");
        bool inputE = Input.GetButtonDown("RotateRight");

        Vector3 delta = new Vector3(inputX, 0, inputY);
        delta.Normalize();
        delta = Quaternion.AngleAxis(mainCamera.transform.rotation.eulerAngles.y, Vector3.up) * delta;
        Vector3 newPos = delta * cameraSpeed * Time.deltaTime + mainCamera.transform.position;
        mainCamera.transform.position = newPos;

        if (inputQ)
        {
            //mainCamera.transform.RotateAround(Vector3.up, 90);
        }
	}
}
