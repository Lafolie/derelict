﻿using UnityEngine;
using System.Collections;

public class ItemCard : MonoBehaviour {
    public Texture2D cardImage;
    protected GUIStyle cardStyle = new GUIStyle();
    public Player owner;
    public int itemId;

	// Use this for initialization
	void Start () {
        SetStyle();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected void SetStyle()
    {
        cardStyle.normal.background = cardImage;
        cardStyle.hover.background = cardImage;
        cardStyle.active.background = cardImage;
    }

    public void Draw(Vector2 offset)
    {
        bool clicked = GUI.Button(new Rect(offset.x, offset.y, 150, 200), "", cardStyle);
        if (clicked)
        {
            Use();
        }
    }

    public virtual void Use()
    {

    }
}
