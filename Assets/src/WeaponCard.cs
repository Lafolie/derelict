﻿using UnityEngine;
using System.Collections;

public class WeaponCard : ItemCard {
    public int damage = 1;
    public int range = 1;

	// Use this for initialization
	void Start () {
        SetStyle();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Use()
    {
        HumanPawn p = (HumanPawn)owner.controlledPawn;
        p.EquipItem(this);
    }
}
