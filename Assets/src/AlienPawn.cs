﻿using UnityEngine;
using System.Collections;

public class AlienPawn : Pawn {
    public int hasMoved = 0;
    public int hasAttacked = 0;
    WhiteTile highlight;

    public GeneCard weaponGene;

    void Awake()
    {
        highlight = gameObject.GetComponentInChildren<WhiteTile>();
        HideHighlight();
    }

	// Use this for initialization
	void Start () {
        maxhp = Random.Range(8, 16);
        hp = maxhp;

        weaponGene = (GeneCard)owner.game.SpawnItem(4, owner);
        highlight.game = owner.game;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override int GetAttack()
    {
        return weaponGene.damage;
    }

    public override int GetAttackRange()
    {
        return weaponGene.range;
    }

    public override void SnapToTile(Tile t)
    {
        base.SnapToTile(t);
        highlight.targetTile = t;
    }

    public void ShowHighlight()
    {
        highlight.renderer.enabled = true;
    }

    public void HideHighlight()
    {
        highlight.renderer.enabled = false;
    }

    public override void DrawInfoPane(Vector2 offset)
    {
        base.DrawInfoPane(offset);
        GUISkin skin = owner.game.gameSkin;
        //draw weapon
        GUI.Label(new Rect(offset.x + 20, offset.y + 115, 30, 100), "Nucleus", skin.GetStyle("plainText"));
        weaponGene.Draw(offset + new Vector2(-5, 150));

    }
}
