﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Floor : MonoBehaviour {
    static int maxSize = 16;
    public Section[,] sectionGrid = new Section[maxSize, maxSize];
    public int floorNumber;
    public bool hidden;
    List<Section> sectionList = new List<Section>();
    public GameObject[] sectionPrefabs;
    List<Vector2> openPaths = new List<Vector2>();
    public Tile[] startingTiles = new Tile[4];
    public Section startSection;
    public Derelict game;
    bool canSpawnEnemies = false;


	// Use this for initialization
	void Awake () {
        //for (int x = 0; x <= 2; x++)
        //{
        //    vector3 newpos = transform.position + vector3.forward * 6 * x;
        //    instantiate(sectionprefabs[0], newpos, transform.rotation);
        //}

        //go!
       //GenerateLayout();
       //SetStartingTiles();
	}
	
	// Update is called once per frame
	void Update () {
        //int x = Random.Range(0, 2);
        //print(x);
	}

    Section GetSection(Vector2 gridLocation)
    {
        int x = Mathf.FloorToInt(gridLocation.x);
        int y = Mathf.FloorToInt(gridLocation.y);
        if (x >= 0 && x < maxSize && y >= 0 && y < maxSize)
        {
            return sectionGrid[y, x];
        }
        return null;
    }

    Section SpawnSection(int sectionId, Vector2 gridLocation)
    {
        //break vector for later
        int x = Mathf.FloorToInt(gridLocation.x);
        int y = Mathf.FloorToInt(gridLocation.y);

        //determine location
        Vector3 newPos = transform.position + new Vector3(x, 0, y) * 6;

        //create object and cast
        GameObject newObj = (GameObject)Instantiate(sectionPrefabs[sectionId], newPos, transform.rotation);
        Section newSection = newObj.GetComponent<Section>();
        newSection.floorLocation = gridLocation;

        //add to grid
        sectionGrid[y, x] = newSection;

        //spawn enemies
        float rnd = Random.value;
        if (rnd < 0.25 && canSpawnEnemies)
        {
            int total = Random.Range(2, 5);
            List<Tile> spawnPos = newSection.getRandomTiles(total);
            int alienPlayerIndex = game.players.Length - 1;
            AlienPlayer player = (AlienPlayer)game.players[alienPlayerIndex];
            foreach (Tile t in spawnPos)
            {
                GameObject alienObj = (GameObject)Instantiate(game.alienPawnPrefab, t.transform.position, transform.rotation);
                Pawn alien = alienObj.GetComponent<Pawn>();
                alien.owner = player;
                alien.SnapToTile(t);
                alien.transform.SetParent(newSection.gameObject.transform); //do this to save finding out which enemies to show later
                player.pawnList.Add(alien);
            }
        }

        return newSection;
    }

    void ConnectSections(Section a, Section b, int direction)
    {
        switch (direction)
        {
            case 0:
                a.northExits[0].neighbours[0] = b.southExits[0];
                a.northExits[1].neighbours[0] = b.southExits[1];
                b.southExits[0].neighbours[2] = a.northExits[0];
                b.southExits[1].neighbours[2] = a.northExits[1];
                break;
            case 1:
                a.eastExits[0].neighbours[1] = b.westExits[0];
                a.eastExits[1].neighbours[1] = b.westExits[1];
                b.westExits[0].neighbours[3] = a.eastExits[0];
                b.westExits[1].neighbours[3] = a.eastExits[1];
                break;
            case 2:
                b.northExits[0].neighbours[0] = a.southExits[0];
                b.northExits[1].neighbours[0] = a.southExits[1];
                a.southExits[0].neighbours[2] = b.northExits[0];
                a.southExits[1].neighbours[2] = b.northExits[1];
                break;
            case 3:
                b.eastExits[0].neighbours[1] = a.westExits[0];
                b.eastExits[1].neighbours[1] = a.westExits[1];
                a.westExits[0].neighbours[3] = b.eastExits[0];
                a.westExits[1].neighbours[3] = b.eastExits[1];
                break;
            default:
                break;
        }
    }

    public void GenerateLayout()
    {
        //create the start area
        CreateStartArea();

        //continue generating sections until the map is completed
        bool createdEnd = false;
        int numSections = 0;
        while (openPaths.Count > 0)
        {
            //locals
            numSections++;
            Vector2 pos = openPaths[0];
            Section newSection = null;
            Section[] neighbours = GetAdjacentSections(pos);

            //get exit requirements as int
            int ex = (neighbours[0] && neighbours[0].spawnSouthExitPoint) ? 1 : 0;
            ex += (neighbours[1] && neighbours[1].spawnWestExitPoint) ? 2 : 0;
            ex += (neighbours[2] && neighbours[2].spawnNorthExitPoint) ? 4 : 0;
            ex += (neighbours[3] && neighbours[3].spawnEastExitPoint) ? 8 : 0;

            //check if end section should be spawned
            float exitChance = 0.05f - (openPaths.Count - 1) * 0.01f + numSections * 0.001f;
            if (ex == 4 && !createdEnd)
            {
                float rnd = Random.value;
                if (rnd < exitChance)
                {
                    newSection = SpawnSection(1, pos);
                    createdEnd = true;
                }
            }

            //spawn appropriate tile and connect to neighbours todo: add randomness
            //print(ex);
            int selection = 0;
            List<int> allowed = new List<int>();
            if (!newSection)
            {
                //ugly switch statement. I know of better ways to do this, but whatever, RapidPrototyping!!
                //multiple copies added to allowed for better control of probability
                switch (ex)
                {
                    case 1:
                        //up
                        allowed.Add(2);
                        allowed.Add(4);
                        allowed.Add(4);
                        allowed.Add(4);
                        if (pos.y - 1 < 0)
                        {
                            allowed = new List<int>();
                            allowed.Add(4);
                        }
                        break;
                    case 2:
                        //right
                        allowed.Add(5);
                        allowed.Add(5);
                        if (!neighbours[0] && pos.y - 1 >= 0)
                        {
                            allowed.Add(12);
                            allowed.Add(13);
                            allowed.Add(13);
                        }
                        else if ((neighbours[3] && !neighbours[3].spawnEastExitPoint) || pos.x - 1 < 0)
                        {
                            allowed = new List<int>();
                            allowed.Add(7);
                        }
                        break;
                    case 3:
                        //up right
                        allowed.Add(13);
                        break;
                    case 4:
                        //down
                        if (!neighbours[3] && pos.x - 1 >= 0)
                        {
                            allowed.Add(11); //tleft
                            allowed.Add(11);
                        }
                        if (!neighbours[1] && pos.x + 1 < maxSize)
                        {
                            allowed.Add(12); //tup
                            allowed.Add(12);
                        }
                        if (pos.y + 1 >= maxSize)
                        {
                            if (!createdEnd)
                            {
                                allowed = new List<int>();
                                allowed.Add(1);
                                createdEnd = true;
                            }
                            else
                            {
                                allowed.Add(3);
                            }
                        }
                        else
                        {
                            allowed.Add(2);
                            allowed.Add(2);
                            allowed.Add(2);
                        }
                        break;
                    case 5:
                        //up down
                        allowed.Add(2);
                        break;
                    case 6:
                        //down right
                        allowed.Add(15);
                        break;
                    case 7:
                        //down right up
                        allowed.Add(12);
                        break;
                    case 8:
                        //left
                        allowed.Add(5);
                        allowed.Add(5);
                        if (!neighbours[0] && pos.y + 1 < maxSize)
                        {
                            allowed.Add(11);
                            allowed.Add(14);
                            allowed.Add(14);
                        }else if ((neighbours[1] && !neighbours[1].spawnWestExitPoint) || pos.x + 1 >= maxSize)
                        {
                            allowed = new List<int>();
                            allowed.Add(6);
                        }
                        
                        break;
                    case 9:
                        //left up
                        allowed.Add(14);
                        break;
                    case 10:
                        //left right
                        allowed.Add(5);
                        break;
                    case 11:
                        //left right up
                        allowed.Add(9);
                        break;
                    case 12:
                        //left down
                        allowed.Add(16);
                        break;
                    case 13:
                        //left down up
                        allowed.Add(11);
                        break;
                    case 14:
                        //left down right
                        allowed.Add(10);
                        break;
                    case 15:
                        //left down right up
                        allowed.Add(8);
                        break;
                    default:
                        break;
                }
                //print("ex:" + ex + ", pos:" + pos);
                selection = GetRandomIntFromList(allowed);
                newSection = SpawnSection(selection, pos);
            }

            if (neighbours[0] && neighbours[0].spawnSouthExitPoint)
            {
                ConnectSections(newSection, neighbours[0], 0);
            }
            if (neighbours[1] && neighbours[1].spawnWestExitPoint)
            {
                ConnectSections(newSection, neighbours[1], 1);
            }
            if (neighbours[2] && neighbours[2].spawnNorthExitPoint)
            {
                ConnectSections(newSection, neighbours[2], 2);
            }
            if (neighbours[3] && neighbours[3].spawnEastExitPoint)
            {
                ConnectSections(newSection, neighbours[3], 3);
            }

            //add grid coords to the openPaths list
            if (newSection.spawnNorthExitPoint && !neighbours[0] && pos.y + 1 < maxSize)
            {
                if (!openPaths.Contains(pos + Vector2.up))
                {
                    openPaths.Add(pos + Vector2.up);
                }
            }
            if (newSection.spawnEastExitPoint && !neighbours[1] && pos.x + 1 < maxSize)
            {
                if (!openPaths.Contains(pos + Vector2.right))
                {
                    openPaths.Add(pos + Vector2.right);
                }
            }
            if (newSection.spawnSouthExitPoint && !neighbours[2] && pos.y - 1 >= 0)
            {
                if (!openPaths.Contains(pos - Vector2.up))
                {
                    openPaths.Add(pos - Vector2.up);
                }
            }
            if (newSection.spawnWestExitPoint && !neighbours[3] && pos.x - 1 >= 0)
            {
                if (!openPaths.Contains(pos - Vector2.right))
                {
                    openPaths.Add(pos - Vector2.right);
                }
            }

            newSection.Hide();

            openPaths.RemoveAt(0);

        }

    }

    public Section[] GetAdjacentSections(Vector2 start)
    {
        Section[] result = new Section[4];
        result[0] = GetSection(start + Vector2.up);
        result[1] = GetSection(start + Vector2.right);
        result[2] = GetSection(start - Vector2.up);
        result[3] = GetSection(start - Vector2.right);
        return result;
    }

    void CreateStartArea()
    {
        Section tmp1;
        Section tmp2;
        tmp1 = SpawnSection(0, new Vector2(8, 0));
        tmp2 = SpawnSection(10, new Vector2(8, 1));
        ConnectSections(tmp1, tmp2, 0);
       // tmp1 = SpawnSection(2, new Vector2(5, 2));
        //ConnectSections(tmp2, tmp1, 0);
        openPaths.Add(new Vector2(7, 1));
        openPaths.Add(new Vector2(9, 1));
        startSection = tmp1;
        canSpawnEnemies = true;
    }

    public void SetStartingTiles()
    {
        //add starting tiles to list
        startingTiles[0] = startSection.getTile(new Vector2(0, 0));
        startingTiles[1] = startSection.getTile(new Vector2(1, 0));
        startingTiles[2] = startSection.getTile(new Vector2(0, 1));
        startingTiles[3] = startSection.getTile(new Vector2(1, 1));
        print(startingTiles[0]);
    }

    int GetRandomIntFromList(List<int> values)
    {
        int result;
        int x = Random.Range(0, values.Count);
        x = Random.Range(0, values.Count);
        x = Random.Range(0, values.Count);
        //print("result " + x + "/" + (values.Count));
        result = values[x];
        //print(result);
        
        return result;
    }
}
