﻿using UnityEngine;
using System.Collections;

public class TargetTile : HighlightTile {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnMouseUpAsButton()
    {
        game.attackTarget = targetTile.occupant.GetComponent<Pawn>();
        game.EraseHighlights();
    }

    void OnMouseEnter()
    {
        game.hoverPawn = targetTile.occupant.GetComponent<Pawn>();
    }

    void OnMouseExit()
    {
        if (game.hoverPawn == targetTile.occupant.GetComponent<Pawn>())
        {
            game.hoverPawn = null;
        }
    }
}
