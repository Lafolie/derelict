﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AlienPlayer : Player
{
    int mp = 0;
    ItemCard weapon;
    public List<Pawn> pawnList = new List<Pawn>();

    //constructors
    public AlienPlayer()
        : base()
    {

    }

    public AlienPlayer(int id, Derelict g)
        : base(id, g)
    {

    }

    public AlienPlayer(int id, Derelict g, Pawn newPawn)
        : base(id, g, newPawn)
    {

    }
    public override void StartOfTurn()
    {
        base.StartOfTurn();

        int mpBonus = 3;
        //reset status of pawn
        foreach (Pawn p in pawnList)
        {
            AlienPawn a = (AlienPawn)p;
            a.hasMoved = 0;
            a.hasAttacked = 0;
            if (a.location.renderer.enabled)
            {
                mpBonus = 1;
                a.ShowHighlight();
            }
        }

        mp += mpBonus;
    }

    public override void EndOfTurn()
    {
        base.EndOfTurn();
        foreach (Pawn p in pawnList)
        {
            (p as AlienPawn).HideHighlight();
        }
    }

    public override void DrawInfo(Vector2 offset)
    {
        base.DrawInfo(offset);
        GUI.Label(new Rect(10, 150, 500, 500), mp + "MP", skin.label);
        if (controlledPawn)
        {
            controlledPawn.DrawInfoPane(offset);
        }
    }

    //alien player does not need to call base methods for session handling
    public override void SessionStore()
    {
        AlienSessionData data = new AlienSessionData();
        data.mp = mp;
        data.hand = new List<int>(); //this is far quicker than copying a list
        foreach (ItemCard card in hand)
        {
            data.hand.Add(card.itemId);
        }

        Session.alienStats = data;
    }

    public override void SessionLoad()
    {
        AlienSessionData data = Session.alienStats;
        mp = data.mp;
        hand = new List<ItemCard>();
        foreach (int id in data.hand)
        {
            hand.Add(game.SpawnItem(id, this));
        }
    }


}
