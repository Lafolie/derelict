﻿using UnityEngine;
using System.Collections;

public class WhiteTile : HighlightTile {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnMouseUpAsButton()
    {
        Pawn p = targetTile.occupant.GetComponent<Pawn>();
        p.owner.game.focusPawn = p;
    }

    void OnMouseEnter()
    {
        if (renderer.enabled)
        {
            game.hoverPawn = targetTile.occupant.GetComponent<Pawn>();
        }
    }

    void OnMouseExit()
    {
        if (renderer.enabled && game.hoverPawn == targetTile.occupant.GetComponent<Pawn>())
        {
            game.hoverPawn = null;
        }
    }
}
