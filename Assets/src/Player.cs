﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//does not need a transform, so don't inherit monobehaviour
public class Player 
{
    protected int playerNum;
    public Pawn controlledPawn;
    public Derelict game;
    protected GUISkin skin;
    public List<ItemCard> hand = new List<ItemCard>();

    //proper constructors!
    public Player()
    {

    }
    public Player(int id, Derelict g)
        : base()
    {
        playerNum = id;
        game = g;
        skin = g.gameSkin;
    }

    public Player(int id, Derelict g, Pawn pawn)
        : this(id, g)
    {
        ChangePawn(pawn);
    }

    public virtual void ChangePawn(Pawn newPawn)
    {
        if (controlledPawn)
        {
            //controlledPawn.owner = null;

        }
        controlledPawn = newPawn;
        controlledPawn.owner = this;
        controlledPawn.playerNum = playerNum;

    }

    public virtual void StartOfTurn()
    {

    }

    public virtual void EndOfTurn()
    {
        
    }

    public virtual void DrawInfo(Vector2 offset)
    {
        GUI.Label(new Rect(10, 80, 500, 500), "Player" + (playerNum+1) + "'s Turn", skin.label);

        //draw hand
        Vector2 pos = new Vector2(420, 650);
        foreach (ItemCard c in new List<ItemCard>(hand))
        {
            c.Draw(pos);
            pos = new Vector2(pos.x + 135, pos.y + 5);
        }
    }

    //create struct to be stored in session
    public virtual void SessionStore()
    {
        PlayerSessionData data = new PlayerSessionData();
        data.hp = controlledPawn.hp;
        data.maxhp = controlledPawn.maxhp;
        data.resource = controlledPawn.resource;
        data.maxResource = controlledPawn.resourceMax;
        data.hand = new List<int>(); //this is far quicker than copying a list
        foreach (ItemCard card in hand)
        {
            data.hand.Add(card.itemId);
        }

        Session.playerStats[playerNum] = data;
    }

    //load statistics from session
    public virtual void SessionLoad()
    {
        PlayerSessionData data = Session.playerStats[playerNum];
        controlledPawn.hp = data.hp;
        controlledPawn.maxhp = data.maxhp;
        controlledPawn.resource = data.resource;
        controlledPawn.resourceMax = data.maxResource;
        MonoBehaviour.print(data.hp);
        MonoBehaviour.print(data.hand.Count);
        hand = new List<ItemCard>();
        foreach (int id in data.hand)
        {
            hand.Add(game.SpawnItem(id, this));
        }
    }
}
