﻿using UnityEngine;
using System.Collections;

public class HighlightTile : MonoBehaviour {
    public Derelict game;
    public Tile targetTile;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public virtual void OnMouseUpAsButton()
    {
        game.moveTile = targetTile;
        game.EraseHighlights();
    }
}
