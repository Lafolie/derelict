﻿using UnityEngine;
using System.Collections;

public class HumanPlayer : Player 
{
    //constructors
    public HumanPlayer()
        : base()
    {

    }

    public HumanPlayer(int id, Derelict g)
        : base(id, g)
    {

    }

    public HumanPlayer(int id, Derelict g, Pawn newPawn)
        : base(id, g, newPawn)
    {

    }

    public override void StartOfTurn()
    {
        base.StartOfTurn();
        HumanPawn p = (HumanPawn)controlledPawn;
        p.module.usedThisTurn = false;
    }

    public override void ChangePawn(Pawn newPawn)
    {
        base.ChangePawn(newPawn);
        HumanPawn p = (HumanPawn)newPawn;
        p.UpdateMaterial();
    }

    public override void DrawInfo(Vector2 offset)
    {
        base.DrawInfo(offset);
        controlledPawn.DrawInfoPane(offset);
        
    }

    public override void SessionStore()
    {
        base.SessionStore();
        PlayerSessionData data = Session.playerStats[playerNum];
        data.weapon = (controlledPawn as HumanPawn).weapon.itemId;
        data.equipment = (controlledPawn as HumanPawn).module.itemId;
        Session.playerStats[playerNum] = data;
    }
}
