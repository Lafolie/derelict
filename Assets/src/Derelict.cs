﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Derelict : MonoBehaviour {
    public Controller controller;
    public Camera mainCamera;
    public  Player[] players = new Player[Session.numberOfPlayers];
    int currentPlayerIndex = 0;
    Player currentPlayer;
    int turn = 0;
    public GameObject floorPrefab;
    public GameObject humanPawnPrefab;
    public GameObject alienPawnPrefab;
    public GameObject[] cardPrefabs;
    public GameObject greenTilePrefab;

    public Pawn focusPawn;
    public Pawn hoverPawn;
    GameObject greenTile;
    static Vector3 greenOffset = new Vector3(0, 0.025f, 0);

    //game stats
    int floorNum = Session.floor;
    public Floor floor;

    //turn vars
    List<GameObject> highlightTiles = new List<GameObject>();
    int movePhase = 0;
    int diceRoll = 0;
    public Tile moveTile;
    int attackPhase = 0;
    string attackMsg = "";
    public Pawn attackTarget;
    string turnPhase = "default";

    //gui themes
    public GUISkin gameSkin;

	// Use this for initialization
	void Start () 
    {
        
        //create the alien player
        int alienid = Session.numberOfPlayers - 1;
        players[alienid] = new AlienPlayer(alienid, this);

        //create the other players and pawns
        print(alienid);
        print(players.Length);
        for (int i = 0; i < players.Length - 1; i++)
        {
            GameObject pawnObj = (GameObject)Instantiate(humanPawnPrefab);
            Pawn newPawn = pawnObj.GetComponent<Pawn>();
            players[i] = new HumanPlayer(i, this, newPawn);
            players[i].hand.Add(SpawnItem(1, players[i]));
            players[i].hand.Add(SpawnItem(3, players[i]));

            if (floorNum > 1)
            {
                players[i].SessionLoad();
            }
        }

        //instantiate the floor
        GameObject floorObj = (GameObject)Instantiate(floorPrefab, new Vector3(-48, 0, -48), transform.rotation);
        floor = floorObj.GetComponent<Floor>();
        floor.game = this;
        floor.GenerateLayout();
        floor.SetStartingTiles();

        greenTile = (GameObject)Instantiate(greenTilePrefab);
        currentPlayer = players[0];
	}


	
	// Update is called once per frame
	void Update () 
    {
        if (turn == 0)
        {
            //position pawns in starting tiles
            for (int i = 0; i < players.Length - 1; i++)
            {
                Tile startPos = floor.startingTiles[3-i];
                players[i].controlledPawn.SnapToTile(startPos);
                
                //update stats
                if (floorNum > 1)
                {
                    players[i].SessionLoad();
                }
            }

            if (floorNum > 1)
            {
                players[players.Length - 1].SessionLoad();
            }

            turn++;
        }

        //change alien player's selection
        if (focusPawn && currentPlayer is AlienPlayer && focusPawn.owner == currentPlayer)
        {
            AlienPawn pwn = (AlienPawn)currentPlayer.controlledPawn;
            if (pwn && movePhase == 0 || attackPhase == 0)
            {
                pwn.ShowHighlight();
            }
            else if(pwn)
            {
                pwn.HideHighlight();

            }

            AlienPawn npwn = (AlienPawn)focusPawn;
            if (turnPhase == "default" && (npwn.hasAttacked == 0 || npwn.hasMoved == 0))
            {
                if (pwn)
                {
                    pwn.hasMoved = movePhase;
                    pwn.hasAttacked = attackPhase;
                }
                
                currentPlayer.ChangePawn(focusPawn);
                movePhase = npwn.hasMoved;
                attackPhase = npwn.hasAttacked;
                attackMsg = "";
                
            }
        }

        //update the green tile (could be event based but adding it here)
        if (currentPlayer.controlledPawn)
        {
            greenTile.transform.position = currentPlayer.controlledPawn.transform.position + greenOffset;

        }

        if (turnPhase != "move" && currentPlayer.controlledPawn)
        {
            greenTile.renderer.enabled = true;
        }
        else
        {
            greenTile.renderer.enabled = false;
        }
 
	}

    void NextTurn()
    {
        currentPlayer.EndOfTurn();
        currentPlayerIndex = (currentPlayerIndex + 1 < players.Length) ? currentPlayerIndex + 1 : 0;
        currentPlayer = players[currentPlayerIndex];
        turn += (currentPlayerIndex == 0) ? 1 : 0;
        currentPlayer.StartOfTurn();
        ResetTurnVars();
    }

    void ResetTurnVars()
    {
        movePhase = 0;
        turnPhase = "default";
        moveTile = null;
        attackPhase = 0;
        attackTarget = null;
        attackMsg = "";

        EraseHighlights();
    }

    void OnGUI()
    {
        //always draw
        GUI.Label(new Rect(10, 10, 500, 500), "Floor " + floorNum + ", Turn " + turn, gameSkin.GetStyle("label"));

        //draw player stuff
        currentPlayer.DrawInfo(new Vector2(10, 500));
        if (hoverPawn)
        {
            hoverPawn.DrawInfoPane(new Vector2(900, 500));
        }

        if (currentPlayer.controlledPawn == null)
        {
            movePhase = -1;
            attackPhase = -1;
        }

        //hacky cancelling
        bool cancel = Input.GetButtonDown("Fire2");

        //draw movement button?
        if (turnPhase == "default" || turnPhase == "move")
        {
            bool move;
            switch (movePhase)
            {
                case 0:
                    move = GUI.Button(new Rect(10, 230, 150, 50), "Move Roll", gameSkin.button);
                    if (move)
                    {
                        //logic stuff
                        moveTile = null;
                        movePhase = 1;
                        diceRoll = Random.Range(1, 7);
                        turnPhase = "move";

                        //get movement radius
                        highlightTiles = new List<GameObject>();
                        Section s = currentPlayer.controlledPawn.location.GetComponentInParent<Section>();
                        List<Tile> possibleTiles = s.getTilesInRadius(diceRoll, currentPlayer.controlledPawn.location.sectionLocation, true);
                        foreach (Tile t in possibleTiles)
                        {
                            if ((!t.occupant || t == currentPlayer.controlledPawn.location) && t.renderer.enabled)
                            {
                                GameObject highlight = (GameObject)Instantiate(s.blueHighlight, t.transform.position + Vector3.up * 0.03f, transform.rotation);
                                HighlightTile h = highlight.GetComponent<HighlightTile>();
                                h.game = this;
                                h.targetTile = t;
                                highlightTiles.Add(highlight);
                            }
                            
                        }

                    }
                    break;
                case 1:
                    move = GUI.Button(new Rect(10, 230, 150, 50), "Move Roll", gameSkin.GetStyle("disabled"));
                    GUI.Label(new Rect(160, 230, 200, 50), "You rolled a " + diceRoll + "!", gameSkin.GetStyle("basicText"));
                    if(moveTile)
                    {
                        if (currentPlayer.controlledPawn is HumanPawn)
                        {
                            HumanPawn p = (HumanPawn)currentPlayer.controlledPawn;
                            p.SnapToTile(moveTile);
                        }
                        else
                        {
                            currentPlayer.controlledPawn.SnapToTile(moveTile);
                        }
                        movePhase++;
                        turnPhase = "default";
                    }
                    break;
                default:
                    GUI.Button(new Rect(10, 230, 150, 50), "Move Roll", gameSkin.GetStyle("disabled"));
                    break;
            }
            
        }
        
        //draw attack button?
        if (turnPhase == "default" || turnPhase == "attack")
        {
            bool attack;
            switch (attackPhase)
            {
                case 0:
                    attack = GUI.Button(new Rect(10, 290, 150, 50), "Attack", gameSkin.button);
                    if (attack)
                    {
                        attackTarget = null;
                        turnPhase = "attack";
                        attackPhase++;

                        highlightTiles = new List<GameObject>();
                        Section s = currentPlayer.controlledPawn.location.GetComponentInParent<Section>();
                        List<Tile> possibleTiles = s.getTilesInRadius(currentPlayer.controlledPawn.GetAttackRange(), currentPlayer.controlledPawn.location.sectionLocation, true);
                        foreach (Tile t in possibleTiles)
                        {
                            if (t.occupant && t != currentPlayer.controlledPawn.location && t.renderer.enabled)
                            {
                                GameObject highlight = (GameObject)Instantiate(s.redHighlight, t.transform.position + Vector3.up * 0.03f, transform.rotation);
                                HighlightTile h = highlight.GetComponent<HighlightTile>();
                                h.game = this;
                                h.targetTile = t;
                                highlightTiles.Add(highlight);
                            }
                            
                        }

                        //check that there was a target to prevent getting stuck here
                        if (highlightTiles.Count == 0)
                        {
                            attackPhase = 0;
                            turnPhase = "default";
                        }
                    }
                    break;
                case 1:
                    if (attackTarget)
                    {
                        EraseHighlights();
                        diceRoll = Random.Range(1, 7);
                        int dmg = diceRoll + currentPlayer.controlledPawn.GetAttack();
                        switch (diceRoll)
                        {
                            case 1:
                                attackMsg = "You rolled a 1 and missed!";
                                break;
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                attackTarget.Damage(dmg);
                                attackMsg = "You rolled a " + diceRoll + " for " + dmg + " damage!";
                                break;
                            case 6:
                                attackTarget.Damage(dmg + 6);
                                attackMsg = "Critical! " + dmg + "damage!";
                                break;
                            default:
                                break;
                        }
                        attackPhase++;
                        turnPhase = "default";
                    }
                    else if(cancel)
                    {
                        EraseHighlights();
                        attackPhase = 0;
                        turnPhase = "default";
                    }
                    break;
                default:
                    GUI.Button(new Rect(10, 290, 150, 50), "Attack", gameSkin.GetStyle("disabled"));
                    if (attackMsg != "")
                    {
                        GUI.Label(new Rect(160, 290, 350, 50), attackMsg, gameSkin.GetStyle("basicText"));
                    }
                    break;
            }

        }

        //end turn button
        bool endTurn = GUI.Button(new Rect(10, 350, 150, 50), "End Turn", gameSkin.button);
        if (endTurn)
        {
            NextTurn();
        }
    }

    public void EraseHighlights()
    {
        if (highlightTiles.Count > 0)
        {
            foreach (GameObject t in highlightTiles)
            Destroy(t.gameObject);
        }
        highlightTiles = new List<GameObject>();
    }

    public ItemCard SpawnItem(int id, Player owner)
    {
        GameObject obj = (GameObject)Instantiate(cardPrefabs[id]);
        ItemCard newItem = obj.GetComponent<ItemCard>();
        newItem.owner = owner;
        newItem.itemId = id;
        return newItem;

    }

    public bool CanPlayCards()
    {
        return turnPhase == "default";
    }

    public void AdvanceToNextFloor()
    {
        //store session vars
        foreach (Player p in players)
        {
            p.SessionStore();
        }
        Session.floor++;
        Application.LoadLevel(1);
    }
}
