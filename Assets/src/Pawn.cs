﻿using UnityEngine;
using System.Collections;

public class Pawn : MonoBehaviour {
    //state stuff
    public Player owner;
    public Tile location;
    public int playerNum;

    //statistics
    public string pname = "NewPawn";
    public int hp = 20;
    public int maxhp = 20;
    public int resource = 20;
    public int resourceMax = 20;
    public string resourceName = "";
    public int exp = 0;
    public int level = 1;

	// Use this for initialization
	void Start () {
        SetDefaultStats();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public virtual void SnapToTile(Tile t)
    {
        if (location)
        {
            location.occupant = null;
        }
        location = t;
        t.occupant = gameObject;
        transform.position = t.transform.position;
        t.OnLand(this);
    }

    public virtual void DrawInfoPane(Vector2 offset)
    {
        GUISkin skin = owner.game.gameSkin; //hax
        GUI.Box(new Rect(offset.x, offset.y, 400, 300), pname, skin.box);
        GUI.Label(new Rect(offset.x + 20, offset.y + 40, 300, 100), "HP " + hp + "/" + maxhp, skin.GetStyle("plainText"));
        GUI.Label(new Rect(offset.x + 20, offset.y + 75, 300, 100), resourceName + " " + resource + "/" + resourceMax, skin.GetStyle("plainText"));
    }

    protected void SetDefaultStats()
    {
        hp = 20;
        maxhp = 20;
        exp = 20;
        level = 20;
    }

    public virtual int GetAttack()
    {
        return 0;
    }

    public virtual int GetAttackRange()
    {
        return 1;
    }

    public virtual int GetDefense()
    {
        return 0;
    }

    public virtual void OnMouseUpAsButton()
    {
        owner.game.focusPawn = this;
    }

    void OnMouseEnter()
    {
        owner.game.hoverPawn = this;
    }

    void OnMouseExit()
    {
        if (owner.game.hoverPawn == this)
        {
            owner.game.hoverPawn = null;
        }
    }

    public void Damage(int amt)
    {
        amt -= GetDefense();
        hp = Mathf.Clamp(hp - amt, 0, maxhp);
        if (hp == 0)
        {
            Destroy(gameObject);
        }
    }
}
