﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Section : MonoBehaviour {
    public Section[] neighbours = new Section[4];
    public Tile[,] tileGrid = new Tile[6,6];
    public bool spawnNorthExitPoint;
    public Tile[] northExits = new Tile[2];
    public bool spawnEastExitPoint;
    public Tile[] eastExits = new Tile[2];
    public bool spawnSouthExitPoint;
    public Tile[] southExits = new Tile[2];
    public bool spawnWestExitPoint;
    public Tile[] westExits = new Tile[2];
    public bool hidden = false;
    public GameObject blueHighlight;
    public GameObject redHighlight;
    public Vector2 floorLocation;

	// Use this for initialization
	void Awake () {
        //create the layout map array
        Tile[] sectionTiles = GetComponentsInChildren<Tile>();
        foreach (Tile tile in sectionTiles)
        {
            int x = Mathf.FloorToInt(tile.sectionLocation.x); 
            int y = Mathf.FloorToInt(tile.sectionLocation.y);
            tileGrid[y, x] = tile;
        }

        //update tile neighbours
        updateTileNeighbours();

        //List<Tile> test = getTilesInRadius(4, new Vector2(2, 2));
        //Vector3 spawnOffset = new Vector3(0, 0.025f, 0);
        //foreach (Tile t in test)
        //{
        //    Instantiate(blueHighlight, t.transform.position + spawnOffset, t.transform.rotation);
        //}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public Tile getTile(Vector2 gridPos)
    {
        int x = Mathf.FloorToInt(gridPos.x);
        int y = Mathf.FloorToInt(gridPos.y);

        if (x >= 0 && x <= 5 && y >= 0 && y <= 5)
        {
            return tileGrid[y, x];
        }
        return null;
    }

    Tile[] getAdjacentTiles(Vector2 start)
    {
        Tile[] result = new Tile[4];
        result[0] = getTile(start + Vector2.up);
        result[1] = getTile(start + Vector2.right);
        result[2] = getTile(start - Vector2.up);
        result[3] = getTile(start - Vector2.right);
        return result;
    }

    //depth-first search
    public List<Tile> getTilesInRadius(int radius, Vector2 start, bool ignoreOccupied)
    {
        //grab some memory
        List<Tile> closedList = new List<Tile>();
        List<Tile> openList = new List<Tile>();

        //add starting tile to list
        openList.Add(getTile(start));
        openList[0].movementCost = 0;
        openList[0].parentTile = null;

        //while there are nodes left
        while (openList.Count > 0)
        {
            //get a tile from the open list
            Tile t = openList[0];
            closedList.Add(t); //add to closed list, we've checked it

            //determine cost of tile
            int cost = (t.parentTile) ? t.parentTile.movementCost + 1 : 0;
            t.movementCost = cost;

            //if there's any spaces left...
            if (cost < radius)
            {
                //get adjacent tiles and iterate through them
                Tile[] newTiles = t.getAdjacentTiles();
                for (int i = 0; i < 4; i++)
                {
                    Tile newT = newTiles[i];
                    //check that tile exists and update cost
                    if (newT && (!newT.occupant || ignoreOccupied))
                    {
                        if (openList.Contains(newT) && newT.parentTile.movementCost > cost)
                        {
                            newT.parentTile = t;
                        }
                        else if(!closedList.Contains(newT))
                        {
                            newT.parentTile = t;
                            openList.Add(newT);
                        }
                    }
                }
            }
            openList.RemoveAt(0);

        }
        return closedList;
    }

    public List<Tile> getRandomTiles(int numTiles)
    {
        List<Tile> result = new List<Tile>();
        List<Tile> tiles = new List<Tile>();

        //get list of tiles (probably should keep this)
        foreach(Tile t in GetComponentsInChildren<Tile>())
        {
            tiles.Add(t);
        }

        for (int i = 0; i < numTiles; i++)
        {
            int x = Random.Range(0, tiles.Count);
            result.Add(tiles[x]);
            tiles.RemoveAt(x);
        }
            return result;
    }
    
    public void Reveal()
    {
        foreach(Renderer obj in GetComponentsInChildren<Renderer>())
        {
            obj.enabled = true;
            hidden = false;
        }

        foreach (AlienPawn pawn in GetComponentsInChildren<AlienPawn>())
        {
            pawn.HideHighlight();
        }
    }

    public void Hide()
    {
        foreach(Renderer obj in GetComponentsInChildren<Renderer>())
        {
            obj.enabled = false;
            hidden = true;
        }
    }

    void updateTileNeighbours()
    {
        foreach (Tile tile in tileGrid)
        {
            if (tile)
            {
                Tile[] newNeighbours = getAdjacentTiles(tile.sectionLocation);
                for (int i = 0; i < 4; i++)
                {
                    if (!tile.neighbours[i]) { tile.neighbours[i] = newNeighbours[i]; }
                }
            }
        }
    }
}
