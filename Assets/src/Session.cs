﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct PlayerSessionData
{
    public int hp;
    public int maxhp;
    public int resource;
    public int maxResource;
    public List<int> hand;
    public int weapon;
    public int equipment;

}

public struct AlienSessionData
{
    public int mp;
    public List<int> hand;
    public int weapon;
    public int otherThing;
}

//Session is used to store information across the application instance (ie across unity scenes)
public static class Session
{
    public static int numberOfPlayers = 2;
    public static int floor = 1;
    public static PlayerSessionData[] playerStats = new PlayerSessionData[3];
    public static AlienSessionData alienStats = new AlienSessionData();
}
