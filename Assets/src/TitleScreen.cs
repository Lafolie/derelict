﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour {
    public GUISkin skin;
    bool showRules = false;
	// Use this for initialization
	void Start () {
	    //TODO:
        Session.numberOfPlayers = 3;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void LoadScreen(int players)
    {
        //TODO:
        //  +set persistence playercount
        //  +go to async level load
    }

    void OnGUI()
    {
        //title text
        GUI.Label(new Rect(25, 25, 300, 300), "DERELICT", skin.GetStyle("title"));
        GUI.Label(new Rect(25, 800, 200, 100), "lu003011\n2015 Dale Longshaw", skin.GetStyle("plainText"));

        //menu
        if (!showRules)
        {
            //normal menu
            if (GUI.Button(new Rect(79, 200, 150, 75), "2 Players", skin.button))
            {
                Session.numberOfPlayers = 2;
                Application.LoadLevel(1);
            }

            if (GUI.Button(new Rect(79, 300, 150, 75), "3 Players", skin.button))
            {
                Session.numberOfPlayers = 3;
                Application.LoadLevel(1);
            }

            if (GUI.Button(new Rect(79, 400, 150, 75), "4 Players", skin.button))
            {
                Session.numberOfPlayers = 4;
                Application.LoadLevel(1);
            }

            if (GUI.Button(new Rect(79, 600, 150, 75), "Help", skin.button))
            {
                showRules = true;
            }


        }
        else
        {
            //print rules
            GUI.Label(new Rect(25, 140, 200, 100), "RULES", skin.label);
            GUI.Label(new Rect(25, 220, 800, 100), "Up to 3 Survivors play against the Hivemind as they explore a nearby\nderelict in hopes of finding the parts they need to fix their ship.", skin.GetStyle("basicText"));
            GUI.Label(new Rect(25, 340, 830, 50), "Explore the derelict floor-by-floor, generating new sections as you play.", skin.GetStyle("basicText"));
            GUI.Label(new Rect(25, 400, 750, 50), "On your turn you can move, attack, and play cards from your hand.", skin.GetStyle("basicText"));
            GUI.Label(new Rect(25, 460, 760, 50), "The Hivemind player controls Aliens and must wipe out the Suriviors.", skin.GetStyle("basicText"));
            GUI.Label(new Rect(25, 520, 560, 50), "Reach the Goal tiles to advance to the next floor.", skin.GetStyle("basicText"));


            if (GUI.Button(new Rect(79, 600, 150, 75), "Back", skin.button))
            {
                showRules = false;
            }
        }
    }
}
