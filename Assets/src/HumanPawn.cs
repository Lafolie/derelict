﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HumanPawn : Pawn {
    public Material[] playerMaterials = new Material[3];
    public WeaponCard weapon;
    public ModuleCard module;

	// Use this for initialization
	void Start () {
        SetDefaultStats();
        resourceName = "Power";

        pname = "Player" + (playerNum+1) + "'s Survivor";
        //initial equipment
        weapon = (WeaponCard)owner.game.SpawnItem(0, owner);
        module = (ModuleCard)owner.game.SpawnItem(2, owner);
        
	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    public override void SnapToTile(Tile t)
    {
        base.SnapToTile(t);
        Section sec = location.GetComponentInParent<Section>();
        Section[] neighbours = owner.game.floor.GetAdjacentSections(sec.floorLocation);
        
        if (neighbours[0] && neighbours[0].spawnSouthExitPoint && sec.spawnNorthExitPoint)
        {
            neighbours[0].Reveal();
        }
        if (neighbours[1] && neighbours[1].spawnWestExitPoint && sec.spawnEastExitPoint)
        {
            neighbours[1].Reveal();
        }
        if (neighbours[2] && neighbours[2].spawnNorthExitPoint && sec.spawnSouthExitPoint)
        {
            neighbours[2].Reveal();
        }
        if (neighbours[3] && neighbours[3].spawnEastExitPoint && sec.spawnWestExitPoint)
        {
            neighbours[3].Reveal();
        }
    }

    public void UpdateMaterial()
    {
        foreach (Renderer mesh in GetComponentsInChildren<Renderer>())
        {
            mesh.material = playerMaterials[playerNum];
        }
    }

    public override void DrawInfoPane(Vector2 offset)
    {
        GUISkin skin = owner.game.gameSkin; //hax
        base.DrawInfoPane(offset);

        //draw weapon
        GUI.Label(new Rect(offset.x + 20, offset.y + 115, 30, 100), "Weapon", skin.GetStyle("plainText"));
        weapon.Draw(offset + new Vector2(-5, 150));

        //draw module
        GUI.Label(new Rect(offset.x + 180, offset.y + 115, 30, 100), "Suit Mod", skin.GetStyle("plainText"));
        module.Draw(offset + new Vector2(155, 150));
    }

    public override int GetAttack()
    {
        return weapon.damage;
    }

    public override int GetAttackRange()
    {
        return weapon.range;
    }

    public override int GetDefense()
    {
        return module.defense;
    }

    public void EquipItem(ItemCard card)
    {
        if (owner.game.CanPlayCards())
        {
            if (card is WeaponCard && weapon != card)
            {
                owner.hand.Add(weapon);
                weapon = (WeaponCard)card;
            }
            else if (card is ModuleCard && module != card)
            {
                owner.hand.Add(module);
                module = (ModuleCard)card;
            }
            owner.hand.Remove(card);

        }
        {
            
        }
    }

}
