﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {
    public Vector2 sectionLocation;
    public GameObject occupant;
    public bool exitTile = false;
    public bool startTile = false;
    public bool goalTile = false;
    public bool existsInWorld = false;
    public int movementCost;  //lazily adding this here
    public Tile parentTile; //more lazy pathfinding vars
    public Tile[] neighbours = new Tile[4];

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void LandOn(object chr)
    {

    }

    public Tile[] getAdjacentTiles()
    {
        return neighbours;
    }

    public virtual void OnLand(Pawn pawn)
    {
        if (goalTile && pawn is HumanPawn)
        {
            pawn.owner.game.AdvanceToNextFloor();
        }
    }
}
