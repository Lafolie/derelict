﻿using UnityEngine;
using System.Collections;

public class ModuleCard : ItemCard {
    public int defense = 0;
    public int costPower = 2;
    public bool usedThisTurn = false;
	// Use this for initialization
	void Start () {
        SetStyle();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Use()
    {
        HumanPawn p = (HumanPawn)owner.controlledPawn;
        if (p.module == this && !usedThisTurn && p.resource >= costPower)
        {
            //activate
            p.resource -= costPower;
            p.Damage(-3);
            usedThisTurn = true;
        }
        else
        {
            //equip
            p.EquipItem(this);
        }
    }

}
